#!/bin/bash
#
# This Script requires `curl` and `jq`
#

if [ -z "$ZONE_ID" ] || [ -z "$AUTH_EMAIL" ] || [ -z "$AUTH_KEY" ]; then
    echo "auth not set" >&2
    exit 1
fi
if [ -z "$DOMAINS" ]; then
    echo "domains not set" >&2
    exit 1
fi

CL_CONT="Content-Type: application/json"
CL_AUTH1="X-Auth-Email: $AUTH_EMAIL"
CL_AUTH2="X-Auth-Key: $AUTH_KEY"

# date for log
echo "DDNS UPDATE AT `date`"

# fetch ips
IP4=`curl -s https://api.ipify.org`
IP6=`curl -s https://api64.ipify.org`

# check and print ipv4
if [ -z "$IP4" ]; then
    echo "Cannot fetch IPv4" >&2
    exit 1
fi
echo "Current IPv4: $IP4"

# check and print ipv6
if [ -z "$IP6" ]; then
    echo "Cannot fetch IPv6" >&2
    exit 1
fi
echo "Current IPv6: $IP6"

# check for cached ips
F_IP4='/tmp/cf.ddns.ip4.txt'
F_IP6='/tmp/cf.ddns.ip6.txt'
if [ -r "$F_IP4" ] && [ "`cat "$F_IP4"`" = "$IP4" ] && [ -r "$F_IP6" ] && [ "`cat "$F_IP6"`" = "$IP6" ]; then
    echo "Last cached IPv4 and IPv6 match current ones. No update necessary."
    exit 0
fi
echo "$IP4" > "$F_IP4"
echo "$IP6" > "$F_IP6"

# iterate over records
IFS=',' read -ra RECORDS <<< "$DOMAINS"
for i in ${RECORDS[@]}; do
    IFS='=' read -ra i <<< "$i"
    record_name=${i[0]}
    record_type=${i[1]}

    record_proxy=false
    if [[ "$record_type" =~ ^\!.+$ ]]; then
        record_proxy=true
        record_type=${record_type:1}
    fi

    # check if type is supported & determine required ip
    case "$record_type" in
        AAAA)
            MY_IP=$IP6
            ;;
        A)
            MY_IP=$IP4
            ;;
        *)
            echo "Error: $record_name's record type ($record_type) is not supported" >&2
            continue
            ;;
    esac

    # fetch record data
    res=`curl -s "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records?type=$record_type&name=$record_name"  \
      -H "$CL_CONT" \
      -H "$CL_AUTH1" \
      -H "$CL_AUTH2"`
    cres=`jq -r '.result[0]' <<< $res`
    record_ip=`jq -r '.content' <<< $cres`
    record_id=`jq -r '.id' <<< $cres`
    record_is_proxied=`jq -r '.proxied' <<< $cres`

    # validate ip
    if [ -z "$record_ip" ] || [ "$record_ip" == "null" ]; then
        echo "Cannot fetch ip for $record_name: $(jq -r '.errors[0].message' <<< $res)" >&2
        continue
    fi
    echo "$record_name's IP is $record_ip ($record_type) (proxy: $record_is_proxied)"

    # validate record id format
    if [ -z "$record_id" ] || [ "$record_id" == "null" ] || [[ ! "$record_id" =~ ^[a-zA-Z0-9]{5,}$ ]]; then
        echo "API failure while fetching record id for $record_name ($record_id)" >&2
        continue
    fi

    # check if update is necessary
    if [ "$record_ip" == "$MY_IP" ] && [ "$record_proxy" == "$record_is_proxied" ]; then
        echo "No update necessary, skipping"
        continue
    fi
    echo "Updating to $MY_IP (proxy: $record_proxy) ..."

    # Update!
    cres=`curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$record_id" \
      -H "$CL_CONT" \
      -H "$CL_AUTH1" \
      -H "$CL_AUTH2" \
      --data "{\"type\":\"$record_type\",\"name\":\"$record_name\",\"content\":\"$MY_IP\",\"ttl\":1,\"proxied\":$record_proxy}"`

    if [ "`jq -r '.success' <<< $cres`" != "true" ]; then
        echo "Error while updating:" >&2
        (jq <<< $cres) >&2
        continue
    fi

    echo "Update successful"

done
