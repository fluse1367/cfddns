FROM alpine:latest
WORKDIR /ddns

# install packages
RUN apk add --no-cache bash curl jq

# script
COPY ddns.sh .
RUN chmod +x ddns.sh

# entry
CMD bash -c "echo DDNS every $UPDATE_INTERVAL seconds; while (true); do echo ---[ CF DDNS ]----; sleep $UPDATE_INTERVAL; bash ddns.sh; done"