# CFDDNS - Cloudflare Dyanmic DNS
This image provides a simple bash script for updating Cloudflare DNS A and AAAA entries.

The script is controlled via environment variables.
| Variable | Example | Description |
| --- | --- | --- |
| `UPDATE_INTERVAL` | `20` | How often the script should run	 in seconds |
| `ZONE_ID` | `1f36b892c74166fc016b55fc9707f535` | Cloudflare Zone ID |
| `AUTH_KEY` | `65bb5780bd845ea6a09a5208cc4b7ae77a3e8` | Cloudflare API Key |
| `AUTH_EMAIL` | `your@mail.com` | Cloudflare Account Email |
| `DOMAINS` | `a.example.com=A,b.example.com=!A` | List of domains to update |

The `DOMAINS` variable is in the format `domain=record_type` separated by commas. You can prefix the record type with a `!` to enable the cloudflare proxy for this domain. Valid record types are `A` (IPv4) and `AAAA` (IPv6).

The current IP will be queried from ipify.org and cached, so unnecessary API calls will be avoided and only be made when the ip doesn't match the cached one (so it has changed).

Example docker command:
```
docker run --rm \
-e UPDATE_INTERVAL=20 \
-e ZONE_ID=1f36b892c74166fc016b55fc9707f535 \
-e AUTH_KEY=65bb5780bd845ea6a09a5208cc4b7ae77a3e8 \
-e AUTH_EMAIL=your@mail.com \
-e DOMAINS=a.example.com=A,b.example.com=AAAA,c.example.com=!A,d.example.com=!AAAA
fluse1367/cfddns
```

Example docker compose configuration:
```yaml
version: '3.1'
services:
ddns:
image: 'fluse1367/cfddns'
restart: unless-stopped
environment:
- 'UPDATE_INTERVAL=20'
- 'ZONE_ID=1f36b892c74166fc016b55fc9707f535'
- 'AUTH_KEY=65bb5780bd845ea6a09a5208cc4b7ae77a3e8'
- 'AUTH_EMAIL=your@mail.com'
- 'DOMAINS=a.example.com=A,b.example.com=AAAA,c.example.com=!A,d.example.com=!AAAA'
```